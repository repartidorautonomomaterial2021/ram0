/*Programa de recolección de pulsos de encoder 
 * para después transformarlos a vueltas y mandarlas por
 * i2c en 4 bytes por separado
 * 
 * Autores: Ma. Fernanda Huerta
 *          Oscar Estrada
 *          David Fontecilla
 */

#include <Wire.h>
//pines de interrupción para señales de encoder A y B
int encoderPin1 = 3;
int encoderPin2 = 2;
//variables auxiliares para saber el número de pulsos
volatile int lastEncoded = 0;
volatile long encoderValue = 0;
long lastencoderValue = 0;
int lastMSB = 0;
int lastLSB = 0;
//variable que representa un float de vueltas
char vueltas_array[] = {0,0,0,0};
float vueltas;

void setup() {
  Serial.begin (230400);

  pinMode(encoderPin1, INPUT); 
  pinMode(encoderPin2, INPUT);

  digitalWrite(encoderPin1, HIGH); //turn pullup resistor on
  digitalWrite(encoderPin2, HIGH); //turn pullup resistor on

  //call updateEncoder() when any high/low changed seen
  //on interrupt 0 (pin 2), or interrupt 1 (pin 3) 
  attachInterrupt(0, updateEncoder, CHANGE); 
  attachInterrupt(1, updateEncoder, CHANGE);
  Wire.begin(0x09);
  Wire.onRequest(requestEvent);
  Wire.onReceive(receive);
}

/*función que limpia el número de vueltas si recibe 
 * información del maestro
 */
void receive(int howMany)
{
    int aber = Wire.read();
    if(aber == 5){
    encoderValue = 0;
    }
}
void loop(){}

//función que acumula los pulsos
void updateEncoder(){
  int MSB = digitalRead(encoderPin1); //MSB = most significant bit
  int LSB = digitalRead(encoderPin2); //LSB = least significant bit

  int encoded = (MSB << 1) |LSB; //converting the 2 pin value to single number
  int sum  = (lastEncoded << 2) | encoded; //adding it to the previous encoded value

  if(sum == 0b1101 || sum == 0b0100 || sum == 0b0010 || sum == 0b1011) encoderValue ++;
  if(sum == 0b1110 || sum == 0b0111 || sum == 0b0001 || sum == 0b1000) encoderValue --;

  lastEncoded = encoded; //store this value for next time
}

/*función que calcula las vueltas dependiendo de los pulsos
 * y las divide en 4 bytes para ser mandadas por i2c
 */
void requestEvent(){
  vueltas = (encoderValue/2672.00);
  Serial.println(vueltas);
  byte flotante;
  byte msb;
  byte lsb;
  if(vueltas <0.00){
  int entero_positivo = -1*int(vueltas);
  msb = ((entero_positivo)>>8)&0xFF;
  lsb = (entero_positivo)&0xFF;
  flotante = ((-1*vueltas)-entero_positivo)*100;
  vueltas_array[3] = 1;
  }
  else{
    msb = ((int(vueltas))>>8)&0xFF;
    lsb = (int(vueltas))&0xFF;
    flotante =(vueltas -(int(vueltas)))*100;
    vueltas_array[3]=0;
  }
  vueltas_array[0] = msb;
  vueltas_array[1] = lsb;
  vueltas_array[2] = flotante; 
  Wire.write((byte*)&vueltas_array,sizeof(vueltas_array));
}
