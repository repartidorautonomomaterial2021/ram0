/*Programa que simula al arduino que maneja al sensor IR con su respectivo
 * servomotor. El programa se supone que debería de mandar una 'o' cuando 
 * hay un cambio de estado (obstaculo o camino libre)
 */
void setup() {
  Serial.begin(9600);
}

void loop() 
{
  Serial.println('o');
  delay(5000);
  Serial.println('o');
  delay(1000);
}
