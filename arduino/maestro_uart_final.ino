/*arduino nano que se comunica a través de i2c 
 * con todos los arduinos del sistema:
 * -4 esclavos que leen los pulsos de los encoders
 *  y regresan el número de vueltas
 * 
 * Este arduino manda toda la información a la raspberry pi
 * por medio de uart
 * 
 * Autor: David Fontecilla
 */

#include <Wire.h>                                             //libreria i2c  
/*salidas del maestro para controlar los motores por pwm*/
#define pwm1 5                                                
#define pwm2 6                                               
#define pwm3 9
#define pwm4 10
#define IRdirec 0x0C
                                            
byte enc_array[4]={0x08,0x09,0x0A,0x0B};                      //direcciones esclavos
void setup()
{
  Wire.begin(); // join i2c bus (address optional for master)
  Serial.begin(9600); // start serial for output
  pinMode(pwm1,OUTPUT);
  pinMode(pwm2,OUTPUT);
  pinMode(pwm3,OUTPUT);
  pinMode(pwm4,OUTPUT);
}

/*funcion que toma 4 bytes: 
 * 2 bytes para el entero
 * un byte para los decimales
 * un byte para saber si el número de vueltas que llega es negativo o positivo
 */
float array_to_float(byte msb,byte lsb,byte dec,byte neg)
{
  //variable donde se almacenan los bytes ya transformados a float
  float vueltas;
  vueltas =  ((msb<<8)|lsb)+float(dec/100.00);
  if(neg == 1)
  {
    return vueltas*(-1);
  }
  else if (neg == 0)
  {
    return vueltas;
  }
}
/*funcion que adquiere los 4 bytes de los maestros 
 * para que a partir de esos valores se obtengan las vueltas
 * en una sola variable flotante
 */
float adquirir_i2c(byte enc, uint8_t bytes)
{
  /*array que almacena parcialmente los bytes que llegan de los esclavos para
 * después transformarlos a float
 */
  byte array1[4] = {0,0,0,0};
  Wire.requestFrom(enc, bytes);
  array1[0] = Wire.read();
  array1[1] = Wire.read();
  array1[2] = Wire.read();
  array1[3] = Wire.read();
  return array_to_float(array1[0],array1[1],array1[2],array1[3]);
}

void loop()
{
  if(Serial.available()>0)
  {
   //variable que nos manda la rasp dependiendo de la información que quiera saber 
   char var = Serial.read();
   //Si recibe una "a" es porque la rasp quiere saber los valores de encoders
   if(var == 0x61)
   {
      //arreglo que almacena el numero de vueltas que da cada llanta                                                
      float v_enc[4] = {0.0,0.0,0.0,0.0};
      for(uint8_t i = 0;i<4;i++)
      {
        v_enc[i]=adquirir_i2c(enc_array[i],4);
        //manda las vueltas por uart a la rasp
        Serial.println(v_enc[i]);
        delay(20);
      }
   }
  
   //si manda una "b" es porque quiere resetear los valores de los encoders a 0
   else if (var == 0x62)
   {
      //se manda un dato a los esclavos de encoderc
      for(uint8_t i=0;i<4;i++)
      {
        Wire.beginTransmission(enc_array[i]);
        Wire.write(5);  
        Wire.endTransmission();
      }
   }
  }
  //valores de pwm fijos para corregir el desplazamiento del carrito
  analogWrite(pwm1,254);//d5 --> 0x08--> primer num
  analogWrite(pwm2,253);//d6--> 0x09--> segundo num
  analogWrite(pwm3,253);//d9 --> 0x0B --> cuarto num
  analogWrite(pwm4,254);//d10 --> 0x0A --> tercer num
}
