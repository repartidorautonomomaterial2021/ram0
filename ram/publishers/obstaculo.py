#! /usr/bin/env python

"""
Nodo encargado de comunicarse con el arduino que controla el sensor
IR, este publica en el canal /permiso_avanzar un true si puede avanzar
el carro porque no hay obstaculos, y un false para pararlo en el caso
de que haya un obstaculo
"""
import serial
import rospy
from std_msgs.msg import Bool
import time

dato = serial.Serial('/dev/ttyUSB1',9600)
rospy.init_node("obstaculo_node")

pub = rospy.Publisher("/permiso_avanzar", Bool, queue_size=1)
permiso = Bool()
permiso.data = True
time.sleep(2)
pub.publish(permiso)

while not rospy.is_shutdown():
	puerto_serial = dato.readline()
	puerto_serial = puerto_serial[0].replace("\n","").replace("\n","")
	puerto_serial = str(puerto_serial[0])
	if puerto_serial == 'o':
		permiso.data = not permiso.data
		rospy.logwarn("permiso: "+str(permiso.data))
		pub.publish(permiso)
