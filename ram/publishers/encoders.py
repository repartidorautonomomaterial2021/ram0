#! /usr/bin/env python

import serial
import rospy
from ram.msg import encArray
from std_msgs.msg import Bool
import time
"""
Nodo encargado de comunicarse con el arduino maestro de los encoders
pide la informacion de la distancia recorrida por cada motor
con una frequencia de 3 hz y publica el numero de vueltas de cada llanta
en el topic /encoders
de igual manera esta suscrito a /lencoders, en caso de que se publique
en ese topic se invoca a la funcion callback que manda el comando b 
por serial, el arduino maestro interpreta que hay que resetear los 
encoders
"""
def reset_enc(msg):
	dato.write("b") 
#serial config
dato = serial.Serial('/dev/ttyUSB0',9600)
#publisher config
rospy.init_node("enco_node")
pub = rospy.Publisher("/encoders",encArray,queue_size = 1)
sub = rospy.Subscriber("/lencoders",Bool,reset_enc)
array = encArray()
rate = rospy.Rate(4)
time.sleep(2)
dato.write("b")
lec_encoders = [0.0,0.0,0.0,0.0]
	
def solicitud_enc():
	dato.write("a")
	for r in range(4):
		lec_encoders[r]=dato.readline()
		lec_encoders[r]=lec_encoders[r].replace("\n","").replace("\r","")
		lec_encoders[r]=float(lec_encoders[r])
#		lec_encoders[r]=abs(lec_encoders[r]) 
	return lec_encoders

while not rospy.is_shutdown():
	array.data = solicitud_enc()
	pub.publish(array)
	rospy.loginfo("encoders: "+str(array))
	rate.sleep()
