#! /usr/bin/env python
import rospy
from std_msgs.msg import Bool
from ram.srv import direcSrv, direcSrvResponse
from ram.msg import encArray, camino
import time 
"""
Nodo que tiene dos arreglos: uno define hacia donde moverse y el otro define
cuanta distancia moverse
publica a /cambio_direc ya que detiene el coche una vez que llego a la 
distancia indicada 
publica a /lencoders ya que define cuando acaba la ruta y hay que resetear 
los encoders para empezar de 0 de nuevo
esta suscrito a /encoders ya que checa si el carro ya llego a la distancia 
indicada o no
es servidor /camino_srv ya que el nodo mueve_coche necesita pedirle la 
direccion a donde se tiene que mover en ese momento
"""
cambio = camino()
direccion = "right"
actual_dist = [0.0,0.0,0.0,0.0]
limpiar = Bool()
def send_direc(request):
	dir = direcSrvResponse()
	global direccion
	global cambio
	dir.direc = direccion
	cambio.camino = True
	rospy.loginfo("define camino cambio: "+str(cambio.camino)+str(dir.direc))
	pubPer.publish(cambio)
	return dir.direc


def enccb(msg):
	global actual_dist
	actual_dist = msg.data
#	rospy.loginfo("define camino: "+str(msg.data))

#publishers, subscribers and services
rospy.init_node("camino_node")
service = rospy.Service('/camino_srv',direcSrv,send_direc)
pubPer = rospy.Publisher("/cambio_direc", camino, queue_size = 1)
pubLen = rospy.Publisher("/lencoders",Bool, queue_size = 1)
sub = rospy.Subscriber("/encoders",encArray,enccb)
time.sleep(2)

#camino a seguir
cam_array = ["right","forward","back","left"]
dist_array = [-7.00,-14.00,-7.00,0.00]

#global variables
cam = 0
while not rospy.is_shutdown():
	while not (dist_array[cam]-0.5) <= actual_dist[0] <= (dist_array[cam]+0.5):
		direccion = cam_array[cam]
	rospy.logwarn("define camino llego: "+str(actual_dist[0]))
	cambio.camino = False
	pubPer.publish(cambio)
	if cam <= 2:
		cam = cam+1
	else:
		limpiar.data = True 
		pubLen.publish(limpiar)
		cam = 0
		rospy.loginfo("define camino quiere limpiar encoders")		
