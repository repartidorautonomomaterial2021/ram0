#! /usr/bin/env python
import rospy 
from std_msgs.msg import Bool
from ram.msg import camino
from ram.srv import direcSrv,direcSrvRequest
import RPi.GPIO as GPIO
import time

"""
Nodo que mueve al coche dependiendo de si tiene permiso de moverlo o no
esta suscrito a /permiso_avanzar y /cambio_direc ya que necesita saber
si hay algun obstaculo o ya tiene que detenerse para cambiar de 
direccion, tambien es cliente del servicio /camino_srv
a este le solicita la informacion de la direccion a donde debe avanzar
"""
permiso = True
cambio = True
def camino_change(msg):
	global cambio
	cambio = msg.camino
def permisocb(msg):
	global permiso
	permiso = msg.data
time.sleep(2)
#client config
rospy.init_node("move_node")
rospy.wait_for_service('/camino_srv')
client = rospy.ServiceProxy('/camino_srv',direcSrv)
request = direcSrvRequest()
#rate = rospy.Rate(2)
subObs = rospy.Subscriber("/permiso_avanzar",Bool,permisocb)
subCam = rospy.Subscriber("/cambio_direc",camino,camino_change)
global var
direccion = "right"

#motor drivers

#gpio setup
GPIO.setmode(GPIO.BCM)
GPIO.setup(17, GPIO.OUT)
GPIO.setup(27, GPIO.OUT)
GPIO.setup(23, GPIO.OUT)
GPIO.setup(24, GPIO.OUT)
GPIO.setup(25, GPIO.OUT)
GPIO.setup(22, GPIO.OUT)
GPIO.setup(26, GPIO.OUT)
GPIO.setup(16, GPIO.OUT)

def mot1(dir):
	if dir == "forward":
		GPIO.output(17,1)
		GPIO.output(27,0)
	elif dir == "back":
		GPIO.output(17,0)
		GPIO.output(27,1)
	elif dir == "stop":
		GPIO.output(17,0)
		GPIO.output(27,0)

def mot2(dir):
	if dir == "forward":
		GPIO.output(23,1)
		GPIO.output(24,0)
	elif dir == "back":
		GPIO.output(23,0)
		GPIO.output(24,1)
	elif dir == "stop":
		GPIO.output(23,0)
		GPIO.output(24,0)

def mot3(dir):
	if dir == "forward":
		GPIO.output(25,1)
		GPIO.output(22,0)
	elif dir == "back":
		GPIO.output(25,0)
		GPIO.output(22,1)
	elif dir == "stop":
		GPIO.output(25,0)
		GPIO.output(22,0)

def mot4(dir):
	if dir == "forward":
		GPIO.output(26,1)
		GPIO.output(16,0)
	elif dir == "back":
		GPIO.output(26,0)
		GPIO.output(16,1)
	elif dir == "stop":
		GPIO.output(26,0)
		GPIO.output(16,0)
def left():
	mot1("back")
	mot2("forward")
	mot3("forward")
	mot4("back")

def right():
#	rospy.logwarn("mvoing right")
	mot1("forward")
	mot2("back")
	mot3("back")
	mot4("forward")

def stop():
	mot1("stop")
	mot2("stop")	
	mot3("stop")
	mot4("stop")

def forward():
	mot1("forward")
	mot2("forward")
	mot3("forward")
	mot4("forward")
	
def backward():
	mot1("back")
	mot2("back")
	mot3("back")
	mot4("back")

def move(dir):
	if dir == "right":
		right()
	elif dir == "left":
		left()
	elif dir == "forward":
		forward()
	elif dir == "back":
		backward()
def camino_change(msg):
	global cambio
	cambio = msg.camino

def permisocb(msg):
	global permiso 
	permiso = msg.data

while not rospy.is_shutdown():
	request.direcReq = True
	direccion=client(request)
	rospy.loginfo("mueve coche direccion a moverse: "+str(direccion.direc))
	rospy.loginfo("permiso y cambio: "+str(permiso)+str(cambio))
	while permiso == True and cambio == True:
		move(direccion.direc)
	stop()
#	rate.sleep()
